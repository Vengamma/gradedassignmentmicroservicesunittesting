create database world;
use world;
create table admin(email varchar(50) primary key,password varchar(20));

create table book(bookid int primary key auto_increment,bookauthor varchar(30),bookname varchar(30),bookprice varchar(10));

create table user(userid int primary key auto_increment,username varchar(20),usermail varchar(25),userpassword varchar(15));